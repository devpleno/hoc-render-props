import React from 'react'
import withHttp from './withHttp'

const MyIP = props => {
    if (props.isLoading && !props.data) {
        return <div>Carregando...</div>
    }

    return (
        <div style={props.style}>Meu ip é: {props.data['origin']}</div>
    )
}

export default withHttp('http://httpbin.org/ip')(MyIP)