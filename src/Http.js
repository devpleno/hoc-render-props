import React, {Component} from 'react'
import axios from 'axios'
import PropTypes from 'prop-types'

class Http extends Component {

    constructor(props) {
        super(props)

        this.state = {
            isLoading: true,
            data: null
        }
    }

    componentDidMount() {
        axios.get(this.props.url).then((ret) => {
            this.setState({ isLoading: false, data: ret['data'] })
        })
    }

    render() {        
        return this.props.children(this.state)
    }
}

Http.propTypes = {
    children: PropTypes.func.isRequired,
    url: PropTypes.string.isRequired
}

export default Http