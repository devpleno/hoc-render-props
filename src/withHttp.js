import React from 'react'
import axios from 'axios'

// const withHttp = ({url, propName}) => WrappedComponent => {
const withHttp = url => WrappedComponent => {
    return class extends React.Component {

        constructor(props) {
            super(props)

            this.state = {
                isLoading: true,
                data: null
            }
        }

        componentDidMount() {
            axios.get(url).then((ret) => {
                this.setState({ isLoading: false, data: ret['data'] })
            })
        }

        render() {
            // const props = {
            //     [propName]: {
            //         data: this.state.data,
            //         isLoading: this.state.isLoading
            //     }
            // }

            // return (<WrappedComponent data={this.state.data} isLoading={this.state.isLoading} {...props} />)

            return (<WrappedComponent data={this.state.data} isLoading={this.state.isLoading} {...this.props} />)
        }
    }
}

export default withHttp