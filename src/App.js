import React, { Component } from 'react';

import MyIP from './MyIP';
import MyAgent from './MyAgent';
import MyIPUA from './MyIPUA';
import Card from './Card';
import Http from './Http';

class App extends Component {
  render() {
    return (
      <div>
        <MyIP style={{background: 'red'}} />
        <MyAgent />
        
        <Card header={(state) => <div>Olá {state.counter}</div>} body='conteudo'>
          {state => <div>Children {state.counter}</div> }
        </Card>

        {/*
        <hr />

        <MyIPUA />

        <hr />
        */}
        
        <Http url='https://jsonplaceholder.typicode.com/posts'>
          {statePosts => <Http url='https://jsonplaceholder.typicode.com/users'>
            {stateUsers => {
              if (statePosts.isLoading || stateUsers.isLoading) {
                return <div>Carregando...</div>
              }

              return (
                <div>
                  <p>POSTS: {statePosts['data'][0]['title']}</p>
                  <p>USERS: {stateUsers['data'][0]['name']}</p>
                </div>
              )
            }}
          </Http>}
        </Http>
      </div>
    );
  }
}

export default App;
