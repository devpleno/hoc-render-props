import React, { Component } from 'react'

class Card extends Component {
    constructor(props) {
        super(props)

        this.state = {
            counter: 0
        }

        setInterval(() => {
            this.setState({ counter: this.state.counter + 1 })
        }, 1000)
    }

    render() {
        const { props } = this;

        return (
            <div>
                <h2>{props.header(this.state)}</h2>
                <h5>{props.body}</h5>
                <div>{props.children(this.state)}</div>
            </div>
        )
    }
}

export default Card