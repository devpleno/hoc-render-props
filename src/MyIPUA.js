import React from 'react'
import withHttp from './withHttp'

const MyIPUA = props => {
    if (props.isLoading) {
        return <div>Carregando...</div>
    }

    return (
        <div>
            <div style={props.style}>Meu ip é: {props.data['origin']}</div>
            <div>Meu UA é: {props.data['user-agent']}</div>
        </div>
    )
}

const ComIp = withHttp({
    url: 'http://httpbin.org/ip',
    propName: 'ip'
})(MyIPUA)

export default withHttp({
    url: 'http://httpbin.org/user-agent',
    propName: 'ua'
})(ComIp)