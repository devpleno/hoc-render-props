import React from 'react'
import withHttp from './withHttp'

const MyAgent = props => {
    if (props.isLoading && !props.data) {
        return <div>Carregando...</div>
    }

    return (
        <div>Meu UA é: {props.data['user-agent']}</div>
    )
}

export default withHttp('http://httpbin.org/user-agent')(MyAgent)